/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-22 15:03:00
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 22:06:43
 */
let axios = require("axios");
const { co } = require("co");

let s1 = axios({
    url: "http://121.199.29.84:8001/index/carousel/findAll",
    method: "GET"
});

let s2 = axios({
    url: "http://121.199.29.84:8001/index/category/findAll",
    method: "GET"
});

function* foo() {
    let resp1 = yield s1;
    console.log(resp1.data);
    let resp2 = yield s2;
    console.log(resp2.data);
}
co(foo);