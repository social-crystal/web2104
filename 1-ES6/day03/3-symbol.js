/*
 * @Description: 消除魔术字符串
 * @Author: zzj
 * @Date: 2021-09-18 11:33:29
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-20 19:08:48
 */
function getArea(shape,options){
    let area = 0;
    switch(shape){
        case Shape.SJX:
            area = .5 * options.width * options.height;
            break;
        case Shape.ZFX:
            area = options.width * options.width;
            break;
        case Shape.CIRCLE:
            area = Math.PI * options.r * options.r;
            break;
        default:
            area:-1;
    }
    return area;
}
const Shape = {
    SJX:Symbol("三角形"),
    ZFX:Symbol("正方形"),
    CIRCLE:Symbol("圆")
}
console.log(getArea(Shape.SJX,{width:100,height:100}));
console.log(getArea(Shape.ZFX,{width:100,height:100}));