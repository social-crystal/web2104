/*
 * @Description: 解构应用
 * @Author: zzj
 * @Date: 2021-09-16 14:30:13
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-16 16:52:24
 */

// jquery
let {
    method='get',
    url,
    data={},
    success=function(){return false}
  } = {
    method:'post',
    url:"",
    data:{name:"terry"},
    success:function(){}
    //...
  }
  $.ajax({
    method:'get',
    url:"",
    data:{},
    success:function(){}
    //...
  })
  
  let $ = {
    ajax({
      method='get',
      url,
      data={},
      success=function(){return false}
    }){
      
  
      let xhr = new XMLHttpRequest()
      xhr.open(url,method)
      //...
    }
  }
  
  