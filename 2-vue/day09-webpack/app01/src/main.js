/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-10-19 09:46:17
 * @LastEditors: zzj
 * @LastEditTime: 2021-10-21 17:30:17
 */
import a from "./a"
import b from "./b"
import "./style/common.css"
import "./style/main.scss"

let dom1 = document.createElement("div");
dom1.classList.add("box");
dom1.innerText = JSON.stringify(a);
document.body.appendChild(dom1);

let dom2 = document.createElement("div");
dom2.classList.add("one")
dom2.innerText = JSON.stringify(b);
document.body.appendChild(dom2);
