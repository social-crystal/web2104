/*
 * @Description: 局部作用域
 * @Author: zzj
 * @Date: 2021-09-16 12:07:21
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-17 14:35:07
 */

function foo(){
    {
        //var i = 3;
        //let i = 3;
        (function(){
            var i = 3;
        })();
    }
    console.log(i);
}
foo();