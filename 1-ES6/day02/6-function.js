/*
 * @Description: 箭头函数
 * @Author: zzj
 * @Date: 2021-09-17 11:45:52
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-19 22:02:11
 */

//java 内部类 - 回调 Ajax 对象 数组
let arr = [{
    id:1,
    name:"zhagsan",
    age:11,
    gender:"男"
},{
    id:2,
    name:"wangwu",
    age:12,
    gender:"男"
},{
    id:3,
    name:"zhaohong",
    age:13,
    gender:"女"
}]

//获取所有男生
// let result = arr.filter(function(item){   //过滤，回调函数返回的是布尔类型，将符合对象的对象返回放入空数组
//     return item.gender =="男"
// })
let result = arr.filter(item=> item.gender =="男")  
console.log(result);

//获取所有年龄的数组
let ages = arr.map(function(item){   //map()返回值是一个数组，放的是回调函数返回的值
    return item.age;
}) 
console.log(ages);

//some 返回值为布尔类型，当有一个回调返回true,结果就为true
let isMale = arr.some(item => item.gender ==="男")
console.log("有男生？",isMale);
//every 返回值为布尔类型，当有一个回调返回false,结果就为false
let isAllMale = arr.every(item => item.gender === "男")
console.log("都是男生？",isAllMale);
