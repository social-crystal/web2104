/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-22 14:58:16
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 21:51:39
 */
class ArrayLike{
    constructor(args){
        for(let i=0;i<args.length;i++){
            let val = args[i]
            this[i] = val;
        }  //装换成类数组对象
        Object.defineProperty(this,"length",{
            configurable:true,
            enumerable:false,
            value:args.length
        })  //让类数值中的length不能遍历
        
        //this.length = args.length;
    }
    //请你实现迭代函数
    * [Symbol.iterator](){
        for(let k in this){
            let v = this[k];
            let index = parseInt(k);
            yield {value:this[k],done:index<this.length?false:true};
        }
    }
}
let set = new ArrayLike(["terry","larry","tom"]);
console.log(set);