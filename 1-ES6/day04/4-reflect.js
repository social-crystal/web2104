/*
 * @Description: 函数反射
 * @Author: zzj
 * @Date: 2021-09-22 10:16:55
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 19:22:11
 */
let foo = function (msg) {
    console.log("hello", this.name, msg);
}

let proxy = new Proxy(foo, {
    apply(target, that, args) {
        //target.apply(that, args);  //代理
        Reflect.apply(target,that,args);  //反射
    }
})
proxy.call({ name: "terry" }, "good day");