/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-10-19 09:56:04
 * @LastEditors: zzj
 * @LastEditTime: 2021-10-21 17:25:44
 */
const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
	mode: 'development',
	entry:"./src/main.js",
	output:{
		path:path.resolve(__dirname, "dist"),
		filename:"bundle.js",
		clean:true
	},
	module:{
		rules:[
			{
			test: /\.css$/,
			use:["style-loader","css-loader"]
			},
			{
				test:/\.s[ac]ss$/i,
				use:[
					"style-loader",
					"css-loader",
					"sass-loader"
				],
			},
		]
	},
	plugins: [
		new HtmlWebpackPlugin(),
		new BundleAnalyzerPlugin()
	],
}