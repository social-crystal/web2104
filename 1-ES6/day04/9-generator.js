/*
 * @Description: generator函数
 * @Author: zzj
 * @Date: 2021-09-22 14:32:28
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 21:34:53
 */

function* foo(){
    yield "terry";
    console.log("hello terry");
    yield "larry";
    yield "tom";
}
let result = foo();
//console.log(result);
//console.log(...result);
console.log(result.next());
console.log(result.next());