/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-24 10:18:50
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 16:25:58
 */
class User{
    id:number;
    username:string;
    password:string;
}
interface Foo{   //接口
    login(username:string,password:string):User;  //：代表返回值的类型
}

let obj:Foo;
obj = {
    login(username:string,password:string){
        //登录逻辑
        return new User();
    }
}

export default {};