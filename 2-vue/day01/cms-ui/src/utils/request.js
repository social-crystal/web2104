/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-26 16:38:41
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-26 20:14:22
 */

import axios from "axios";
import qs from "qs";

let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJNalk9Iiwic3ViIjoiYWRtaW4yIiwiaXNzIjoiMDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjYiLCJpYXQiOjE2MzI2NDYxMzIsImF1ZCI6InJlc3RhcGl1c2VyIiwiZXhwIjoxNjMyODE4OTMyLCJuYmYiOjE2MzI2NDYxMzJ9.5wdP1ZyemZ_JapyYcP2TXzOVuqI3CFQFjtWapYUKnWs";

const instance = axios.create({
    baseURL: 'http://121.199.29.84:8001',
    timeout: 10000,
});
instance.interceptors.request.use((config)=>{
    config.headers.Authorization = token;
    return config;
})
instance.interceptors.response.use(({data:resp})=>{
    if(resp.status === 200){
        return resp;
    }
    //alert(resp.message);
    console.log("=====================",resp.message)
    return Promise.reject(resp);
},(error)=>{
    return Promise.reject(error)
})

export function get(url,params) {
    return instance.get(url,{ params });
}
export function post(url,data) {
    data = qs.stringify(data)
    return instance.post(url,data,{
        headers:{
            "Content-Type":"application/x-www-form-urlencoded"
        }
    })
}
export function postJSON(url,data) {
    return instance.post(url,data)
}
export function del(url,params) {
    return instance.delete(url,{params})
}
export default instance;