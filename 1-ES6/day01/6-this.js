/*
 * @Description: this
 * @Author: zzj
 * @Date: 2021-09-16 14:40:52
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-16 14:45:48
 */

/**
 let n = 2;
 toString.call(n) "2"
 n.toString()  "2"
 */
function toString() {
    return this+"";
}
Object.prototype.myToString = toString;

let a = 12;
console.log(typeof a.myToString(),a.myToString());
 let {myToString} = a;
 let n = myToString.call(a);
 console.log(typeof n,n);