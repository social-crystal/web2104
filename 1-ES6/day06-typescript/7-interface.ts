/*
 * @Description: 接口
 * @Author: zzj
 * @Date: 2021-09-24 11:39:05
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 17:33:03
 */
class User{
    id:number;
    name:string;
    gender:string;
    constructor(id:number,name:string,gender:string){
        this.name = name;
        this.id = id;
        this.gender = gender;
    }
}
interface IBaseService{
    foo():void;
}
interface IUserService{
    login(username:string,password:string):User;
    register(user:User):void;
}

class UserServiceImpl implements IUserService, IBaseService{
    foo():void{

    }
    login(username: string, password: string): User{
        return new User(1,"zzj","女")
    }
    register(user: User): void{
        
    }
}