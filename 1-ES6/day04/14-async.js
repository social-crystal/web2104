/*
 * @Description: async
 * @Author: zzj
 * @Date: 2021-09-22 15:19:08
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 22:11:35
 */
let axios = require("axios");

let s1 = axios({
    url: "http://121.199.29.84:8001/index/carousel/findAll",
    method: "GET"
});

let s2 = axios({
    url: "http://121.199.29.84:8001/index/category/findAll",
    method: "GET"
});

async function foo() {
    let resp1 = await s1;
    console.log(resp1.data);
    let resp2 = await s2;
    console.log(resp2.data);
}
foo();