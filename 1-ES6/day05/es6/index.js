/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-24 14:13:16
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-24 14:24:25
 */
import {foo,msg,num} from "./a.js"  //引入必须加文件后缀
import obj from "./a.js"
console.log("index模块中", num)
setTimeout(()=>{
    console.log("index模块中", num)
},1500)

console.log(foo);
console.log(msg);
console.log(obj);