/*
 * @Description: 模拟jQuery
 * @Author: zzj
 * @Date: 2021-09-17 17:06:32
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-20 16:11:11
 */
let jQuery ={};
(function($){
    $.get = function({
        url,
        params={},
        headers = {},
        success
    }){
        let xhr = new XMLHttpRequest();
        url = url + "?"+Qs.stringify(params);
        xhr.open("GET",url);
        for(let k in headers){
            let v = headers[k];
            xhr.setRequestHeader(k,v);
        }
        xhr.send();
        
        xhr.onreadystatechange = function(){
            if(this.readyState == 4){
                if(this.status ==200){
                    let resp =JSON.stringify(this.response)
                    success.call(this,resp)
                }
            }
        }
    }
// $.post   json / x-www-form-urlencode
$.postJSON = function(){}

$.post = function(){}


})(jQuery);