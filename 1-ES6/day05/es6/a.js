/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-24 14:13:10
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-24 14:33:48
 */
console.log("---a模块---")
export function foo(){
    console.log("this foo");
}
export let msg = "hello";
export let num = 1;
console.log("a模块中，初始值",num)
setTimeout(()=>{
    ++num;
    console.log("a模块中",num)

},1000)

export default {}  //export default可以不加声明