/*
 * @Description: Array
 * @Author: zzj
 * @Date: 2021-09-17 11:07:47
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-19 21:16:16
 */
let arr = [{
    id:1,
    name:"zhangsan",
    age:11
},{
    id:2,
    name:"lisi",
    age:12
},{
    id:3,
    name:"wangwu",
    age:13
}]

// let keyIterator = arr.keys();  //迭代器
// console.log(keyIterator);
// let n = keyIterator.next();
// console.log(n);

console.log("--扩展运算符--")
let keys = arr.values();
//console.log(keys);
console.log([...keys]);

console.log("--next--")
//迭代器遍历
let next ;
while(!(next = keys.next()).done){   //判断done是否为false,如果为false,此时while为true,可以执行循环体
    //console.log(next);
    console.log(next.value);//获取值
}