/*
 * @Description: 对象代理
 * @Author: zzj
 * @Date: 2021-09-22 09:50:10
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 19:08:06
 */
let obj = {
    name:""
}

let proxy = new Proxy(obj,{
    set(target,key,val){
        console.log("setter.....")
        target[key] = val;
    },
    get(target,key){
        console.log("getter.....")
        return target[key]
    }
})

proxy.name = "terry";    //调用set方法
//console.log(obj.name);
console.log(proxy.name);   //调用set、get方法
