/*
 * @Description: 迭代器
 * @Author: zzj
 * @Date: 2021-09-22 14:12:53
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 21:25:11
 */
let set = new Set(["terry","larry","tom","jacky"])
//let iterator = set[Symbol.iterator];  //得到自身的迭代器，迭代器没有触发
//let iterator = set[Symbol.iterator]();  //迭代器函数运行
//let iterator = set.keys();  //获取所有key的迭代器
let iterator = set.values();  //得到values的迭代器
console.log(iterator);

let item;
while(!(item = iterator.next()).done){
    console.log(item.value);
}
