/*
 * @Description: 抽象类
 * @Author: zzj
 * @Date: 2021-09-24 11:23:32
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 16:57:24
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Parent = /** @class */ (function () {
    function Parent() {
    }
    Parent.prototype.work = function () {
        console.log("工作");
    };
    Parent.prototype.eat = function () {
        console.log("真香~");
    };
    return Parent;
}());
var Child = /** @class */ (function (_super) {
    __extends(Child, _super);
    function Child() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Child.prototype.study = function () {
        _super.prototype.eat.call(this);
        _super.prototype.work.call(this);
        return "好好学习，天天向上";
    };
    return Child;
}(Parent));
var child = new Child();
child.study();
