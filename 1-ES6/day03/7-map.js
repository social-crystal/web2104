/*
 * @Description: 将对象转换为map,map的方法
 * @Author: zzj
 * @Date: 2021-09-21 16:41:17
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-21 16:52:57
 */
let obj = {
    name:"terry",
    age:12
}
let entries = Object.entries(obj);
let map = new Map(entries);
console.log(map);
map.set("gender","male")
console.log(map.size);

