/*
 * @Description: set、get拦截
 * @Author: zzj
 * @Date: 2021-09-22 09:46:09
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 18:57:04
 */
let obj = {
    _name:""
}
Object.defineProperty(obj,"name",{
    set(val){
        console.log("----set-----")
        this._name = val;
    },
    get(){
        console.log("------get-----")
        return this._name;
    }
})
 
obj.name = "terry";
console.log(obj.name);