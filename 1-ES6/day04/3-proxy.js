/*
 * @Description: 函数调用劫持
 * @Author: zzj
 * @Date: 2021-09-22 10:00:12
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 19:15:11
 */
let foo = function (msg) {
    console.log("hello",this.name,msg);
}
//foo("good day")
//foo.call({name:"terry"},"good day")  //为this.name赋值
let proxy = new Proxy(foo,{
    apply(target,that,args){
        // console.log(target);  //方法
        // console.log(that);    
        // console.log(args);    //实参
        target.apply(that,args);
    }
})
proxy.call({name:"terry"},"good day");