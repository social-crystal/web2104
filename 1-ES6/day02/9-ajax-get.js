/*
 * @Description: ajax GET方式
 * @Author: zzj
 * @Date: 2021-09-17 16:23:39
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-20 15:59:45
 */

//请求接口中的所有轮播图数据
//http://121.199.29.84:8001/swagger-ui.html
//1.初始化xhr
let xhr = new XMLHttpRequest();
//2.设置请求行
let url = "http://121.199.29.84:8001/carousel/findAll";
let method = "GET";
xhr.open(method,url);
//3.设置请求头
let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJNalk9Iiwic3ViIjoiYWRtaW4yIiwiaXNzIjoiMDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjYiLCJpYXQiOjE2MzIwNjE5NTQsImF1ZCI6InJlc3RhcGl1c2VyIiwiZXhwIjoxNjMyMjM0NzU0LCJuYmYiOjE2MzIwNjE5NTR9.D5bj6rryoK2jZeGyNYinrMG6gG6-WQhDCdDEvZo2UVo";
xhr.setRequestHeader("Authorization",token)
//4.设置请求体，并发送请求
xhr.send();
//5.监听响应
xhr.onreadystatechange = function(){   //监听就绪状态的改变
    if(this.readyState == 4){
        if(this.status ==200){
            let response = JSON.parse(this.response);   //凡是带parse的都是对字符串进行解析
            console.log("success:",this.response);
        }else{
            console.log("error:",this.response);
        }
    }
}