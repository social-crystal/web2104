/*
 * @Description: Object.getOwnPropertySymbols()
 * @Author: zzj
 * @Date: 2021-09-20 19:02:56
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-20 19:08:21
 */

//Object.getOwnPropertySymbols()  获取某个对象中所有的symbol属性名
let s1 = Symbol("telephonr");
let obj = {
    name:"terry",
    age:12,
    [s1]:"15826549843",
    [Symbol("email")]:"234658.com"
}
for(let k in obj){
    console.log(k);
}
let ss = Object.getOwnPropertySymbols(obj);
console.log(ss);
console.log(obj[ss[1]]);