/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-26 16:38:41
 * @LastEditors: zzj
 * @LastEditTime: 2021-10-20 14:32:05
 */

import axios from "axios";
import qs from "qs";
import { ElMessage } from 'element-plus';


const instance = axios.create({
    baseURL: 'http://121.199.29.84:8001',
    timeout: 10000
});
instance.interceptors.request.use((config) => {
    let token = localStorage.getItem('token')
    if (token) {
        config.headers.Authorization = token;
    }
    return config;
})
instance.interceptors.response.use(({ data: resp }) => {
    if (resp.status === 200) {
        return resp;
    }
    ElMessage({ type: "error", message: resp.message });
    return Promise.reject(resp)
}, (error) => {
    ElMessage({ type: "error", message: '交互异常' });
    return Promise.reject(error)
})


// url {id:1}
export function get(url, params) {
    return instance.get(url, { params })
}
export function post(url, data) {
    data = qs.stringify(data)
    return instance.post(url, data, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    })
}
export function postJSON(url, data) {
    return instance.post(url, data)
}
export function del(url, params) {
    return instance.delete(url, { params })
}
export default instance;
