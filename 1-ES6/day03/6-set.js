/*
 * @Description: map的方法
 * @Author: zzj
 * @Date: 2021-09-21 16:00:02
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-21 16:48:21
 */
let set = new Set([1,2,3,4,5,1,2,3]);
set.add({});
set.add({});  //空对象的地址不一样
set.delete([...set][6]);
console.log(set.size);
// for (let s of set){
//     console.log(s);
// }