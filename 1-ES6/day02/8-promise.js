/*
 * @Description: 异步操作进行封装
 * @Author: zzj
 * @Date: 2021-09-17 16:11:49
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-19 22:24:38
 */

let promise = new Promise((resolve,reject)=>{
    console.log("start....")
    setTimeout(()=>{
        let random = Math.random();
        console.log("结果",random);
        if(random>0.5){
            resolve(random);
        }else{
            reject(random);
        }
    },1000)
    console.log("end...")
})

promise.then((d)=>{   //当promise变为成功状态时执行
    console.log("成功了....",d)
}).catch((error)=>{   //捕获异常
    console.log("失败了....",error)
});