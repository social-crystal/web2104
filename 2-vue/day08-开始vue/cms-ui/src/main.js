/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-10-20 10:22:54
 * @LastEditors: zzj
 * @LastEditTime: 2021-10-20 10:57:39
 */
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus'

let app = createApp(App)
app.use(store)
app.use(router)
app.use(ElementPlus)
app.mount('#app')
