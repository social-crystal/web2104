/*
 * @Description: 函数默认值
 * @Author: zzj
 * @Date: 2021-09-17 11:31:14
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-19 21:38:50
 */

//参数解构
//对象解构
function foo({name,age,gender="男"}){
    console.log("my name is",name,"my age is",age,"my gender is",gender);
}
foo({age:1})  //按需传入

//数组解构
function bar([name,age,gender="男"]){
    console.log("my name is",name,"my age is",age,"my gender is",gender);
}
bar(["terry",,"女"]) //按序传入

//普通参数
function baz(name,age,gender="男"){
    console.log("my name is",name,"my age is",age,"my gender is",gender);
}
baz("jacky",12)  //声明了几个参数，传几个参数

//rest参数
function abb(a,...b){
    console.log(a);
    console.log(b);
}
abb("terry","larry","tom")