/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-24 11:09:55
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 16:55:19
 */
class Animal{
    name:string;
    age:number;
    constructor(name:string,age:number){
        this.name = name;
        this.age = age;
    }
    sayName():void{
        console.log("my name is ",this.name);
    }
    sayAge():void{
        console.log("my age is ", this.age);
    }
}
class Dog extends Animal{
    color:string;
    constructor(name:string,age:number,color:string){
        super(name,age);
        this.color = color;
    }
    sayColor() {
        console.log("my color is ", this.color);
    }
    sayName() {
        console.log("汪汪~ my name is ", this.name);
    }
}
//泛型
let a:Animal = new Dog("大白",3,"white");
a.sayName();
//a.sayColor();  //报错

let c: Animal = new Animal("huahua", 3);
c.sayName();

let b:Dog = new Dog("小黑",2,"black");
b.sayName();
b.sayColor();