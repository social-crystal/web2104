/*
 * @Description: promise静态方法
 * @Author: zzj
 * @Date: 2021-09-22 11:20:06
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 20:44:58
 */
let axios = require("axios");

let s1 = axios({
    url :"http://121.199.29.84:8001/index/carousel/findAll",
    method:"GET"
});

let s2 = axios({
    url : "http://121.199.29.84:8001/index/category/findAll",
    method: "GET"
});

// allSettled()
let promise = Promise.allSettled([s1, s2]);
promise.then(result => {
    console.log("----------结束-------------")
    console.log(result)
})

//any()实验状态，不考虑

/*  race()
let promise = Promise.race([s1,s2]);
promise.then(result =>{
    console.log(result)
})
*/

/* all()
let promise = Promise.all([s1,s2])
promise.then(result => {    //成功时调用
    console.log("请求全部成功！");
    console.log(result);
}).catch(error =>{   //失败时调用
    console.log("有异常发生！");
    console.log(error);
}).finally(()=>{   //状态改变时调用
    console.log("结束....")
})
*/