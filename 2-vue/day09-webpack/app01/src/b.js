/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-10-19 09:46:11
 * @LastEditors: zzj
 * @LastEditTime: 2021-10-19 09:54:57
 */
import _ from "lodash"

let obj = {
	name: "tom",
	age: 15,
	gender: "male"
}
export default _.cloneDeep(obj)