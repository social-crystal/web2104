/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-10-19 11:13:32
 * @LastEditors: zzj
 * @LastEditTime: 2021-10-21 16:28:03
 */
const path = require("path")
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
	mode:"development",
	entry:{
		main: "./src/main.js",
		home:"./src/home.js",
	},
	output:{
		path: path.resolve(__dirname, "dist"),
		filename:"bundle_[name].js",
		clean:true   //清理之前打包的包
	},
	plugins:[
		new BundleAnalyzerPlugin(),
	],
	optimization:{
		splitChunks: {
			chunks:"all"
		}
	}
}