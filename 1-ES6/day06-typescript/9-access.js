"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
/*
 * @Description: 访问修饰符
 * @Author: zzj
 * @Date: 2021-09-24 11:55:30
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 17:37:26
 */
var Animal = /** @class */ (function () {
    function Animal(id, name) {
        this.id = id;
        this.name = name;
    }
    Animal.prototype.sayName = function () {
        console.log("my name is", this.name);
    };
    Animal.prototype.getId = function () {
        return this.id;
    };
    Animal.prototype.getName = function () {
        return this.name;
    };
    return Animal;
}());
var Dog = /** @class */ (function (_super) {
    __extends(Dog, _super);
    function Dog(id, name) {
        return _super.call(this, id, name) || this;
    }
    Dog.prototype.foo = function () {
        _super.prototype.sayName.call(this);
    };
    return Dog;
}(Animal));
new Dog(2, "小黑").foo();
var a = new Animal(1, "大白");
console.log(a.getId());
console.log(a.getName());
exports["default"] = {};
