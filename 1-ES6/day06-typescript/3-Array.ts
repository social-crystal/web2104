/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-24 10:00:02
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 16:15:44
 */
let arr: Array<string>;  //泛型
arr = ["张三","李四","王五"];
console.log(arr);

let set: Set<number>;
set = new Set();  //声明出来不能直接用
set.add(1);
set.add(2);
console.log(set);


let map: Map<string,number>;  //<键，值>
map = new Map();  
map.set("one",1);
console.log(map);

