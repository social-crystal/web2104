/*
 * @Description: Object定义属性
 * @Author: zzj
 * @Date: 2021-09-17 09:32:20
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-17 18:53:30
 */
let obj = {
    name:"terry",
    age:12
}
Object.defineProperty(obj,"weight",{  //自定义属性
    configurable:true,  //是否可配
    enumerable:false,  //是否遍历,不可遍历不意味着不可访问
    value:50
})
console.log(obj);
console.log(obj.weight);
