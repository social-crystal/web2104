/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-24 09:48:43
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 16:02:28
 */
let a: Object;
a = 2;  //自动装箱
console.log(a);

let b:Object = 3;
console.log(b);

class Goods{
    id:number;
    name:string;
    price:number;
}
let kqs: Goods = { id: 1, name: "矿泉水", price: 2.5 };
let fbm: Goods = { id: 2, name: "方便面", price: 3 };
console.log(kqs);
console.log(fbm);

export default {}