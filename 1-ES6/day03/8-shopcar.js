/*
 * @Description: 购物车
 * @Author: zzj
 * @Date: 2021-09-21 16:53:29
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-24 14:57:25
 */
class Goods{
    constructor(id,name,price){
        this.id = id;
        this.name = name;
        this.price = price;
    }
}
let fbm = new Goods(1,"康师傅方便面",3);
let kqs = new Goods(2,"怡宝矿泉水",2.5);


class Shopcar{
    car ;  //购物车
    constructor(){
        // <goods,number>
        this.car = new Map();
    }
    //向购物车中添加商品
    add(goods,num){
        if(this.car.has(goods)){
            let n = this.car.get(goods);
            this.car.set(goods,n+num)
        }else{
            this.car.set(goods,num)
        }
    }
    //从购物车中移除商品
    remove(goods){
        this.car.delete(goods);
    }
    //购物车中改变商品数量
    edit(goods,num){
        if (this.car.has(goods)) {
            this.car.set(goods, num)
        }
    }
    //清空购物车
    clear(){
        this.car.clear();
    }
    //结算
    sum(){
        let money = 0;
        for(let [goods,num] of this.car){
            money += (goods.price * num);
        }
        return money;
    }
    //查看购物车
    list(){
        for(let item of this.car){
            console.log(item);
        }
    }
}
//为用户分配一个购物车
let shopCar = new Shopcar();
//查看购物车
shopCar.list();
shopCar.add(fbm, 2)
shopCar.add(kqs, 1)
shopCar.add(kqs, 1);
shopCar.list();
console.log("------------")
shopCar.remove(fbm);
shopCar.list();
console.log("------------")
console.log(shopCar.sum());