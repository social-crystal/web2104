var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
 * @Description:
 * @Author: zzj
 * @Date: 2021-09-24 11:09:55
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 16:55:19
 */
var Animal = /** @class */ (function () {
    function Animal(name, age) {
        this.name = name;
        this.age = age;
    }
    Animal.prototype.sayName = function () {
        console.log("my name is ", this.name);
    };
    Animal.prototype.sayAge = function () {
        console.log("my age is ", this.age);
    };
    return Animal;
}());
var Dog = /** @class */ (function (_super) {
    __extends(Dog, _super);
    function Dog(name, age, color) {
        var _this = _super.call(this, name, age) || this;
        _this.color = color;
        return _this;
    }
    Dog.prototype.sayColor = function () {
        console.log("my color is ", this.color);
    };
    Dog.prototype.sayName = function () {
        console.log("汪汪~ my name is ", this.name);
    };
    return Dog;
}(Animal));
//泛型
var a = new Dog("大白", 3, "white");
a.sayName();
//a.sayColor();  //报错
var c = new Animal("huahua", 3);
c.sayName();
var b = new Dog("小黑", 2, "black");
b.sayName();
b.sayColor();
