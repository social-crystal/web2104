/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-10-20 10:22:54
 * @LastEditors: zzj
 * @LastEditTime: 2021-10-20 14:29:57
 */
import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Category from '../views/home/category/CategoryList'
import Article from '../views/home/article/ArticleList'
import ArticlePublish from '../views/home/article/ArticlePublish'
import ArticleDetails from '../views/home/article/ArticleDetails'


const routes = [{
  path: '/',
  redirect: '/home'
}, {
  path: '/register',
  component: Register
}, {
  path: '/login',
  component: Login
}, {
  path: '/home',
  component: Home,
  children: [{
    path: 'category',
    component: Category,
  }, {
    path: 'article',
    component: Article
  }, {
    path: 'article/publish',
    component: ArticlePublish
  }, {
    path: 'article/details',
    component: ArticleDetails
  }]
  // component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
}]



const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  // 白名单
  let paths = ["/login", "/register"]
  if (paths.indexOf(to.path) != -1) {
    next();
  }
  // 验证token
  let token = localStorage.getItem('token');
  console.log('监测token', token);
  if (!token) {
    next('/login')
  } else {
    next();
  }
})


export default router
