/*
 * @Description: 枚举
 * @Author: zzj
 * @Date: 2021-09-24 12:09:20
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 17:48:29
 */
// enum Gender {
//     MALE="男",
//     FEMALE = "女"
// }

enum Gender{
    MALE = 1,
    FEMALE
}

// console.log(Gender.FEMALE);
// console.log(Gender.MALE);

console.log(Gender[1]);  //双向绑定
console.log(Gender[2]);

class User {
    gender:Gender = Gender.FEMALE;
}


export default {}