/*
 * @Description: Object.assign() 
 * @Author: zzj
 * @Date: 2021-09-16 19:08:40
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-16 19:15:44
 */

let p1 = { page: 2, pageSize: 10 };
let p2 = { title: '新冠'};
let p = {...p1,...p2};
console.log(p);
let p3 = Object.assign(p1,p2); //将p1、p2拷贝到p3中，但是p1也会发生改变，相当于对p1进行浅拷贝，p2进行深拷贝
console.log(p3);
console.log(p1);