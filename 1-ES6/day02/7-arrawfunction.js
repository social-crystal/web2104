/*
 * @Description: 箭头函数this指向
 * @Author: zzj
 * @Date: 2021-09-17 12:10:48
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-19 22:15:37
 */

function fool(){
    function bar(){
        console.log("bar:",this);  
    }
    //bar();  //指向全局
    bar.call(obj);  //指向obj
}
let obj = {name: "terry"};
fool.call(obj);


function foo(){
    let bar = () => {   //箭头函数指向包含他的外部函数的this   
        console.log("bar:",this);
    }
    bar.call(obj);   //所以bar指向global
}
let obj1 = {name: "terry"};
foo(obj1);  //foo的this指向global


let obj2 = {
    length :1,
    foo:function(){
      console.log(this.length);
    }
  }
  /*obj2.foo  指向的一个匿名函数 ，本质是一个指针，等价于function(){
      console.log(this.length);
    } */  
  let arr = [1,2,3,obj2.foo,5]  
  arr[3](); //通俗点说就是arr.3() ===> arr.obj2.foo()  //this指向函数名，再指向.前的obj2,最后指向arr