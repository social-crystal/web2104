/*
 * @Description: 抽象类
 * @Author: zzj
 * @Date: 2021-09-24 11:23:32
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 16:57:24
 */

abstract class Parent{
    abstract study():string;  //抽象方法
    work(){
        console.log("工作");
    }
    eat(){
        console.log("真香~");
    }
}
class Child extends Parent{
    study():string{
        super.eat();
        super.work();
        return "好好学习，天天向上"
    }
}
let child:Child = new Child();
child.study();
