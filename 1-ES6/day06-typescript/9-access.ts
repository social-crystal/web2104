/*
 * @Description: 访问修饰符
 * @Author: zzj
 * @Date: 2021-09-24 11:55:30
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 17:37:26
 */
class Animal{
    private id:number;
    private name:string;
    constructor(id:number,name:string){
        this.id = id;
        this.name = name;
    }
    protected sayName(){
        console.log("my name is",this.name);
    }
    public getId(){
        return this.id;
    }
    public getName(){
        return this.name;
    }
}

class Dog extends Animal{
    constructor(id: number, name: string) {
        super(id,name);
    }
    foo(){
        super.sayName();
    }
}
new Dog(2,"小黑").foo();

let a = new Animal(1,"大白");
console.log(a.getId());
console.log(a.getName());

export default {}