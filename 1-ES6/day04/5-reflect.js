/*
 * @Description: 对象反射
 * @Author: zzj
 * @Date: 2021-09-22 10:19:11
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 19:25:17
 */
let obj = {
    name: ""
}

let proxy = new Proxy(obj, {
    set(target, key, val) {
        console.log("setter.....")
        //target[key] = val;  //代理
        Reflect.set(target,key,val);  //反射
    },
    get(target, key) {
        console.log("getter.....")
        //return target[key]  //代理
        return Reflect.get(target, key)  //反射
    }
})

proxy.name = "terry";
//console.log(obj.name);
console.log(proxy.name);   //调用get方法
