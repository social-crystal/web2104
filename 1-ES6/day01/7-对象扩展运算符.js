/*
 * @Description: 对象扩展运算符
 * @Author: zzj
 * @Date: 2021-09-16 18:31:27
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-16 18:53:12
 */

//...将对象中的每一项拿出来放进一个新的对象中，对象的引用地址发生改变，但是每一项的地址不变
let obj = {name:"terry",age:12,gender:"male"};
let{...o} = obj;
console.log(o);  //{ name: 'terry', age: 12, gender: 'male' }
console.log(o === obj);  //false

let o2 = {name:"terry",phone:15512344321,tel:36987};
let {name,...o3} = o2;
console.log(name);  //terry
console.log(o3);  //{ phone: 15512344321, tel: 36987 }  ...将剩余属性打包为一个对象传入o3
console.log(o2.phone === o3);  //false


let s1 = {name:"terry",age:12};
let s2 = {...s1}
console.log(s2);
console.log(s1 === s2);  //false
console.log(s1.name === s2.name);  //true

let s3 = {name:"terry",arr:[1,2,3]};
let s4 = {...s3};
console.log(s4);
console.log(s3.arr === s4.arr);  //true
console.log(s3.name === s4.name);  //true

