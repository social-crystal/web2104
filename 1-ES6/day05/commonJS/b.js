/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-23 11:15:33
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-23 20:18:44
 */
class Animal {
    constructor(name) {
        this.name = name;
    }
    sayName() {
        console.log("my name is ", this.name);
    }
}

module.exports = Animal;
