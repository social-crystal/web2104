"use strict";
/*
 * @Description: 枚举
 * @Author: zzj
 * @Date: 2021-09-24 12:09:20
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 17:48:29
 */
// enum Gender {
//     MALE="男",
//     FEMALE = "女"
// }
exports.__esModule = true;
var Gender;
(function (Gender) {
    Gender[Gender["MALE"] = 1] = "MALE";
    Gender[Gender["FEMALE"] = 2] = "FEMALE";
})(Gender || (Gender = {}));
// console.log(Gender.FEMALE);
// console.log(Gender.MALE);
console.log(Gender[1]); //双向绑定
console.log(Gender[2]);
var User = /** @class */ (function () {
    function User() {
        this.gender = Gender.FEMALE;
    }
    return User;
}());
exports["default"] = {};
