/*
 * @Description: 对象解构
 * @Author: zzj
 * @Date: 2021-09-16 12:18:48
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-16 16:16:56
 */
let {
    // name:name,//这种样式可以直接写成name,
    // age:age
    name,
    age,
    email, //获取未定义的属性名，得到undefined
    gender = "男"  //为属性定义默认值，当解析的属性没有对应值时，显示默认值
} = {
    name:"zzj",
    age:12,
    telephone:15588888888
}
console.log(name,age,email,gender);