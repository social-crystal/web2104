/*
 * @Description: 闭包
 * @Author: zzj
 * @Date: 2021-09-16 12:10:52
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-17 14:39:08
 */

function foo(){
    let a = 3;
    setTimeout(function(){  //属于BOM
        a++;
        console.log("inner:",a)
    },3000)
    console.log("foo end...");
}
foo();