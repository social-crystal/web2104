/*
 * @Description: 模拟axios
 * @Author: zzj
 * @Date: 2021-09-17 17:06:32
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-20 17:05:51
 */
let axios ={};
(function($){
    $.get = function(url,{params={},headers = {} }){
        return new Promise((resolve,reject)=>{
        let xhr = new XMLHttpRequest();
        url = url + "?"+Qs.stringify(params);
        xhr.open("GET",url);
        for(let k in headers){
            let v = headers[k];
            xhr.setRequestHeader(k,v);
        }
        xhr.send();
        
        xhr.onreadystatechange = function(){
            if(this.readyState == 4){
                if(this.status ==200){
                    let resp =JSON.stringify(this.response)
                    resolve(resp)  //标识承诺成功
                }else{
                    let error = JSON.parse(this.response)
                    reject(error)  //标识承诺失败
                }
            }
        }
        })
    }
// $.post   json / x-www-form-urlencode
$.postJSON = function(){}

$.post = function(){}


})(axios);