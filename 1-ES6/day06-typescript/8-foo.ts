/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-24 11:50:41
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 17:34:41
 */
class Animal{
    id:number = 0;
    name:string = "";
    gender?:string;  //gender可有可无
}
let a:Animal = {
    id:1,
    name:"大白"
}
let b:Animal = {
    id:2,
    name:"小黑",
    gender:"male"
}
console.log(a,b);

export default {}