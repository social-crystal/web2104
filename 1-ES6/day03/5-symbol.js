/*
 * @Description: Symbol.hasInstance
 * @Author: zzj
 * @Date: 2021-09-20 19:13:16
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-20 19:45:40
 */

function Student(id,name){
    this.id = id;
    this.name = name;
  }
  let instanceMethod = function(stu) {
    if(stu.id>10){
      return true;
    }
    return false;
  };
  Student[Symbol.hasInstance] = instanceMethod
  console.log(Student[Symbol.hasInstance] === instanceMethod);
  Student[Symbol()] = 'hello world'
  console.log(Object.getOwnPropertySymbols(Student));
  let obj = {}
  obj[Symbol.hasInstance] = instanceMethod;
  
  let s1 = new Student(1,'terry');
  let s2 = new Student(12,'larry');
  console.log('terry',s1 instanceof obj);
  console.log('larry',s2 instanceof obj);
  