/*
 * @Description: 面向对象
 * @Author: zzj
 * @Date: 2021-09-23 10:10:25
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-23 20:05:51
 */

class Animal{
    name = "";   //成员属性   -声明在实例中
    age = 0;
    static num = 1;  //静态属性
    constructor(name,age){   //构造函数
        this.name = name;
        this.age = age;
    }
    sayName(){   //成员方法  -声明在原型中
        console.log("my name is",this.name)
    }
    static sayNum(){    //静态方法  只能访问静态属性
        console.log(this.num);   //this指向类
    }
}
let a1 = new Animal("大白",5);
console.log(a1);
a1.sayName();
let a2 = new Animal("小黑", 3);
console.log(a2);
a2.sayName();
//a2.sayNum();  //发生错误
Animal.sayNum();

class Dog extends Animal{
    gender;
    constructor(name,age,gender){
        super(name,age);
        this.gender = gender
    }
    sayGender(){
        console.log("my gender is",this.gender)
    }
}
let d1 = new Dog("花花",4,"male");
console.log(d1)
Dog.sayNum();  //继承的时候会继承静态方法