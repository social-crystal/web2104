前端 课程：
  任务职责
    重构（ui -> 网页 （html/css/））
    交互（js【ajax】）
    局部dom更新   dom操作

  1. axure（产品经理）
    产品设计
  2. ps（美术）
    UI设计
  3. 重构阶段
    html5(块、行内、功能[form/iframe])
    css3(选择器、选择器优先级、规则【文本、字体、列表...、align-items】、布局【浮动、伸缩盒、】)
  4. dom框架阶段
    linux (软件开发者 linux、 开发 部署)
      后台接口  jar  
      前端  apache
    Javascript
      ECMAScript5  标准
        变量、数据类型、运算符、流程控制语句、String 、Object、Array、函数、正则、面向对象（继承）

        Array.isArray()   静态方法（类，构造函数方法）
        Array.prototype.push()  实例方法（成员方法）
        Array.prototype.shift()  实例方法（成员方法）

        JS弱类型
        var a = 3;
        a = true;
      DOM         浏览器提供的操作dom节点的api
      BOM         
        ajax  异步
    jquery/bootstrap/h5api
  5. 企业级开发阶段
    ECMAScript6
      语法标准：
        开发者
        执行者（nodejs / 浏览器）
    1) hello world
      1. 安装nodejs
      2. 查看版本 
        > node --version
        v14以上
      3. 第一个程序
        1) nodejs交互界面
        2) 编写代码，执行
          $ vi hello.js
          $ node hello.js
    2) 注释
        //
        /**/
    3) 变量声明
      弱类型
      var 奇葩（其他语言）
        1. 重复声明
          var a;
          var a;
        2. 变量的提升
          console.log(d);
          var d;
        3. 没有局部作用域
          function foo(){
            var result = 0;
            for(var i=0;i<=100;i++){
              result += i;
            }
            console.log(result)
            console.log(i);
          }
      let 
        1. 不可重复声明
        2. 不存在变量提升
        3. 具有局部作用域
      const 
        常量，只能赋值一次的变量，其他与let保持相同特性
        let a = 3;
        a++;  // 4
        const b = 3;
        b++;  //error

        const obj = {age:1}  //const保持引用地址不变，obj不变，内容可以变
        obj.age++;  // age 2   
      es5局部作用域
        函数内部作用域，可以通过函数的立即执行来模拟。
    4) 解构
      使用模式匹配方式从一个对象、数组中快速获取值的方式
      let obj = {
        name:"terry",
        age:12,
        gender:"male",
        telephone:"18812344321",
        email:"licy@briup.com"
      }
      let a = obj.name;
      let b = obj.age;
      let c = obj.gender;
      1. 对象解构
        let {
          name:name,
          age:age
        } = {
          name:"terry",
          age:12,
          gender:"male",
          telephone:"18812344321",
          email:"licy@briup.com"
        }
        let {name,age,gender} = {name:"terry",age:12}
        let {name,age,gender='男'} = {name:"terry",age:12}

        let {toString} = 2;
        let {push} = [1,2,3]
        // 自动装箱 let n = new Number(2)
        ☆let n = 2;
          let {myToString} = n
          myToString.call(n)  '2'
          n.toString();   '2'
        rest   ...
          let{name,...other} = {name:"terry",
          age；12}
      2. 数组解构
        let [a,b,c] = [1,2,3]        //[ 1, 2, 3 ]
        let [a,b,c,d] = [1,2,3]      //[ 1, 2, 3, undefined ]
        let [a,b,c,d='5'] = [1,2,3]  //[ 1, 2, 3, '5' ]
        let [a,b] = [1,[3,4]]        // [ 1, [ 3, 4 ] ]
        let [a,[b]] = [1,[3,4]]      // [ 1, [ 3 ] ]
    5) 字符串拓展 String
      1. 可迭代的对象(遍历器接口)
          for...of  ：每一次获取到字符串中的元素
          //对比for...in:  每一次获取到字符串中的索引
      2. API(应用程序编程接口)拓展
        1) 静态方法（声明在构造函数）
        2) 实例（成员）方法（声明在原型对象中）
          includes()：返回布尔值，表示是否找到了参数字符串。
            let s = 'Hello world!';
            s.includes('o') // true
          startsWith()：返回布尔值，表示参数字符串是否在原字符串的头部。
            s.startsWith('Hello') // true
          endsWith()：返回布尔值，表示参数字符串是否在原字符串的尾部。
            s.endsWith('!') // true
          repeat()
            'hello'.repeat(2) // "hellohello"
          padStart() && padEnd()
            'x'.padStart(5, 'ab') // 'ababx'
            'x'.padStart(4, 'ab') // 'abax'
            'x'.padEnd(5, 'ab') // 'xabab'
            'x'.padEnd(4, 'ab') // 'xaba'
          trimStart() && trimEnd()   去掉空格
            let s = '  abc  ';
            s.trim() // "abc"
            s.trimStart() // "abc  "
            s.trimEnd() // "  abc" 
          matchAll()  //返回一个正则表达式在当前字符串的所有匹配
          replaceAll()  //node不能直接运行
            'aabbcc'.replace('b', '_')     // 'aa_bcc'   //不改变原值，当原值出现多个元素时，只能改变一个元素
            'aabbcc'.replace(/b/g, '_')    // 'aa__cc'  //利用正则表达式改变所有要改变的元素
            'aabbcc'.replaceAll('b', '_')  // 'aa__cc'
    6) Number扩展  //只对数值有效
      Number.isNaN  只有对于NaN才返回true，非NaN一律返回false
        Number.isNaN(NaN) // true
        Number.isNaN(15) // false
      Number.isInfinite  对于非数值一律返回false
        Number.isFinite(0.8); // true
        Number.isFinite(NaN); // false
        Number.isFinite(Infinity); // false
      Number.parseInt
        Number.parseInt('11',2) // 3  以二进制计算后返回，同理第二位参数还有8,16
        Number.parseInt('12.34') // 12
      Number.parseFloat
        Number.parseFloat('123.45#') // 123.45
      Number.isInteger  用来判断一个数值是否为整数。
        Number.isInteger(25) // true
        Number.isInteger(25.1) // false
    7)对象 Object
      1. 对象简写
        let name = "terry"
        let obj = {name}
        function foo(){}
        let obj = {
          name:"terry",
          foo,
          sayName(){

          }
        }

        let url = "/article/deleteById"
        let id = 1;
        $.get(url,{id})
      2. 扩展运算符    
        解构赋值
          1）
            let{...o} = obj;  //...将对象中的每一项拿出来放进一个新的对象中，对象的引用地址发生改变，但是每一项的地址不变
          2）
            let o2 = {name:"terry",phone:15512344321,tel:36987};
            let {name,...o3} = o2;  // 先将name属性传入新的对象，然后...将剩余属性打包再传入o3
            console.log(o3);  //{ phone: 15512344321, tel: 36987 }
        扩展运算符
          对象的扩展运算符（...）用于取出参数对象的所有可遍历属性，拷⻉到当前对象之中。 对象的扩展运算符等同于使用Object.assign()方法。            
      3.API
        Object.is()
          ⽤于⽐较两个值是否相等，与严格⽐较运算符（===）的⾏为基本⼀致。不同之处只有两 个：⼀是 +0 不等于 -0 ，⼆是 NaN 等于⾃身。
        Object.assign()   半深拷贝
          ⽤于对象的合并，将源对象（source）的所有可枚举属性，复制到⽬标对象（target）。 如果源对象中的对象存在嵌套关系，进⾏浅拷⻉ 
            let p1 = { page: 2, pageSize: 10 }
            let p2 = { title: '新冠'}

            let p3 = Object.assign(p1,p2);//将p1、p2拷贝到p3中，但是p1也会发生改变，相当于对p1进行浅拷贝，p2进行深拷贝

            //正确写法  监控param的改变 param 参数 
            let param = { 
              page: 2, 
              pageSize: 10 ,
              title: '新冠'
            }
            param = {
              ...param,
              page:3
            }
            //或者
            param = Object.assgin({},param)
        Object.keys(obj)       键的数组  返回指定对象的所有键
        Object.values(obj)     值的数组  返回指定对象的所有值
        Object.entries(obj);   键值对组成的数组的数组  二维数组
        Object.setPrototypeOf(obj,prototype)  //为某个对象设置原型
        Object.getPrototypeOf(obj)  //获取某个对象的原型
        让类数组可以调用所有的数组方法？
      4. 速写
        let name = "terry"
        let age = 12;
        let sayName = function(){}

        let obj = {
          name:name,
          age:age,
          sayName:sayName
        }

        let obj = {
          name,
          age,
          sayName
        }   
    8)原型
      每个函数都有一个原型对象与之对应，函数中有个指针
      prototype指向这个原型，原型中有个指针constructor指向函数

      一个函数的特性与其调用方式有关，如果通过new来调用，这个函数可以理解为构造函数；如果通过()、apply、call来调用，这个函数可以理解为普通函数
        
      function Student(name,age){
        this.name = name;  //this内部属性
        this.age = age;
      }
      Student();  //this指向全局
      new Student();  //this指向新构建的对象(实例)
      Student.call({}); //this指向第一个值，空对象

      new Number()  创建数值的实例对象
      Number()      将其他数据类型转换为Number

      let arr = new Array()
      arr instanceof Object
    9)数组 Array
      let arr = new Array(2)  //一个参数表示数组长度为2
      let arr = new Array(2，3)  //多个参数表示数组有两个元素，分别为2,3
      静态 Array.xxx
        Array.from()
          将类数组对象、set转换为数组
        Array.of()  
          将参数放置到数组内并且返回
      非静态  Array.prototype.xxx
        Array.prototype.keys()  //对键名的遍历  
        Array.prototype.values()  //对键值的遍历
        Array.prototype.entries()  //对键值对的遍历
        Array.prototype.includes()  //判断数组中是否含某个元素
        Array.prototype.flat(Infinity)  //将多维数组转为一维数组
        这三个方法返回值为迭代器对象
          迭代器对象的调用：
            1. for-of
            2. iterator.next()
            3. 拓展运算符(...)
        flat()
          将嵌套的数组“拉平”，变成⼀维的数组。返回⼀个新数组，对原数据没有影响。 
          默认只会“拉平”⼀层，如果想要“拉平”多层的嵌套数组，可以将 flat() ⽅ 法的参数写成⼀个整数，表示想要拉平的层数，默认为1。 
          如果不管有多少层嵌套，都要转成⼀维数组，可以⽤ Infinity 关键字作为参数。
    10) 函数拓展
      1) 默认值
        普通默认值，按序传入
        参数解构，按心意传入
      2) rest参数
        扩展运算符的逆运算
          ...在左边是聚到一起，在右边是打散
            let arr = [1,2,3]
            let n = [9,8,7,...arr]
            let [a,...b] = [1,2,3,4,5]

            function foo(a,...b){
              console.log(a);
              console.log(b);
        }
      3)箭头函数
        如果形参只有一个的话，小括号可以省略；如果函数体只包含了一句返回语句，大括号和return可以省略
          let result = arr.filter(function(item){
          return item.gender =="男"
          })
          可以写成：
          let result = arr.filter(item=> item.gender =="男")
          console.log(result);
        箭头函数指向包含他的外部函数的this   
    11)promise  承诺对象
      浅学：
        对异步操作进行封装
        状态：待定pending、成功resolve、失败reject

        1)构造函数
          let p = new Promise((resolve,reject)=>{   //回调函数用到箭头函数

          })

        2)静态方法
          promise.xxx

        3)实例方法
          promise.prototype.xxx

        ajax  
          GET /carousel/findAll HTTP/1.1
          Authorization:XXWEREAWFEWAFAEW
      深入：
        异步操作的解决方案，常用于封装AJAX
        1.axios
          axios 就是基于promise对AJAX的封装
            0)特点
              XMLHttpRequest(浏览器)、http(nodejs)
              默认将data(post)转换为json
              params(get)
              支持promise

          1)nodejs中应用(http)
            安装cnpm(只能安装一次)
            $ npm install -g cnpm --registry=https://registry.npm.taobao.org
            $ npm init
            $ cnpm install axios --save
          2)实例化        
            配置属性
              {
                method,
                url,
                params
                data
              }

            
          3)全局配置
          4)拦截器
          5)快捷方法
        2.原生ajax的封装

          function loadArticles(){
            //待定
              return new Promise((resolve,reject)=>{
                let xhr = new XMLHttpRequest()
                xhr.open()
                xhr.setRequestHeader()
                xhr.send()
                xhr.onreadystatechange = function(){
                  if(this.readyState === 4){
                    if(this.status === 200){
                      //待定 -> 成功  (执行then中的回调)
                       resolve(this.response);
                    } else{
                      //待定 -> 失败  (执行catch中的回调)
                      reject(this.response);
                    }
                  }
                }
              })
          }
          loadArticles().then().catch().finally()
        3.成员方法
          then/catch/finally   返回值都是一个承诺对象

          Promise.prototype.then(successHandler[,errorHandler])  //成功
          
          Promise.prototype.catch(errorHandler)  //失败
          等价于
          Promise.prototype.then(null,errorHandler) 

          Promise.prototype.finally(handler)  //只要状态确认改变了，就会执行
        4.静态方法
          Promise.all([s1,s2])
            该方法返回值为promise,当s1,s2,....全部执行完毕并且状态为resolved的时候，promise的then才会被调用，该then的回调函数的参数是s1,s2,....的运行结果
            案例：  
              当查询完所有的班级，渠道后在调用查询学生的接口；查询足额生的时候默认查询第一个班级
              Promise.all(s1,s2).then(result => {
                let defaultClass = result[0].data.data[0]
                loadStudent();
              }) 
          Promise.race(iterable)
            返回值为promise,返回率先改变状态的promise结果
          Promise.allSettled(iterable)
            返回值为promise，与all不同，当所有的承诺对象状态被确认的时候会执行promise的then,then的参数为结果，结果有可能成功也有可能失败
          Promise.resolve()
            直接返回一个承诺对象，并且状态为成功
            new Promise.resolve(resolve =>{
              resolve(val);
            })
          Promise.reject()
            直接返回一个承诺对象，并且状态为失败
            new Promise((resolve,reject) =>{
              reject(error);
            })
          iterable  可迭代的对象，常见表示数组/Set....,  可迭代对象中的元素是promise的实例
    12) Symbol   基本数据类型
          函数，无法使用new来调用，只能用在属性名上，每次执行都可以产生式一个唯一的值（基本数据类型），这个值用来作为属性名。
          eg:
            let s1 = Symbol()
            let s2 = "name"
            let obj = {
              [s1]:"terry",    // 神秘的唯一值:"terry"
              [s2]:"tom" ,     // name:"terry"
              s3:"jacky"       // s3 :"jacky"
            }
            obj[s1] 
            obj.name
            obj.s3
          1. Symbol() 函数
            用来产生唯一值
          2. Symbol(flag) 
            flag字符串，表示标识
          3. Symbol.for(flag)   Symbol值重复利⽤
            可以做到使⽤同⼀个 Symbol 值。它接受⼀个字符串作为参数，然后 搜索有没有以该参数作为名称的 Symbol 值。如果有，就返回这个 Symbol 值，否则就新建⼀ 个以该字符串为名称的 Symbol 值，并将其注册到全局。
              eg：
                let s1 = Symbol("s1");
                let s2 = Symbol("s2");
                let s3 = Symbol("s1");
                s1  ===  s2  //false
                s1  ===  s3   //true
          4. Object.getOwnPropertySymbols()
            可以获取某个对象中所有的symbol属性名
          5. 系统内置Symbol
            1) Symbol.hasInstance
              所有的构造函数都内置这个方法
              当其他对象使⽤ instanceof 运算符，判断是否为该对象的实例时，会调⽤这个⽅法
              let obj = {
                [ Symbol.hasInstance]:function(o){
                  return false
                }
              }
              let o = {}
              {} instanceof obj;
            2) Symbol.iterator
              当迭代对象的时候会被调用 for-of
              for(let o of arr){  }
    13) 集合
      是对数组和对象的拓展
      1. Set
        不可以存放相同的值，不可以通过索引来访问。Set是一种特殊的map
        let arr = [1,2,3,4,5,1,2,3]
        let arr = ['terry','larry']
        1) 构造函数
          new Set()
        2) 原型
          Set.prototype.size  //获取容器中元素的个数
          Set.prototype.add()  //向集合中添加一个元素,不允许相同的元素出现
          Set.prototype.delete()
          Set.prototype.has(value)  //判断集合中是否包含value
          Set.prototype.clear()  //清空set集合
          Set.prototype.keys()  //获取set集合key值的迭代器，由于set集合没有key所有与values⽅法返回结果⼀致
          Set.prototype.values()  //获取set集合的value值的迭代器
          Set.prototype.entries()  //获取set集合的entries迭代器
          Set.prototype.forEach()  //与数组类似的迭代⽅法
      2. Map
        键值对，键可以为任意数据类型；
        Map可以提供额外的api

        购物车（家政）
          goods 
            id    name    price
            fbm = {1   北京方便面  2}
            kqs = {2   怡宝矿泉水  2.5}
          let shopcar = new Map();
          shopcar.set(1,1)
          shopcar.set(2,3)

        如何将一个对象转换为map?
        1) 构造函数
          let map = new Map([[],[]])

        2) 原型方法
          Map.prototype.size  //获取集合键值对个数
          Map.prototype.set(key, value)  //向集合中设置⼀个键值对
          Map.prototype.get(key) //从集合中通过key获取value 
          Map.prototype.has(key) //判断集合中是否包含key指定的键 
          Map.prototype.delete(key) //通过key删除⼀个键值对 
          Map.prototype.clear() //清空map集合 
          Map.prototype.keys() //获取map集合key值的迭代器 
          Map.prototype.values() //获取map集合value值的迭代器 
          Map.prototype.entries() //获取map集合entry的迭代器 
          Map.prototype.forEach() //迭代
    14)代理 Proxy
      1.对象   setter/getter
        let obj = {}    //目标对象
        let proxy = new Proxy(obj,{    //代理
          set(target,key,val){
            target[key] = val;
          },
          get(target,key){
            return target[key];
          }
        })
        proxy.name = "terry";   //面向代理使用
      2.函数  apply
        let foo = function(msg){console.log(msg);}
        let proxy = new Proxy(foo,{
          apply(target,that,args){
            target.apply(that,args)
          }
        })
      3.构造函数   constructor
        let Person = function(name){this.name = name;}
        let proxy = new Proxy(Person){
          constructor(target,args){
            return new target(...args)
          }
        }
    15)反射
      与代理一一对应，为代理提供操作方法：操作set/操作get/操作apply/操作constructor
        let proxy = new Proxy(obj,{    
          set(target,key,val){
           Reflect.set(target,key,val);
          },
          get(target,key){
             return Reflect.get(target,key)
          }
        })     
    16)迭代器
      惰性求值的机制，当每次执行next(),才会得到内容
      0.执行过程
        next() 调用 -> 指针
      1.ES6可迭代的对象
        数组、字符串、Set、map
        实例对象可以直接访问Symbol.iterator,构造函数实现了Symbol.iterator接口

        // 标准
        inteface Iterator {
          [Symbol.iterator]():void;
        }
        // 类 == 构造函数 Array String Set Map
        class Array implements Iterator{
          [Symbol.iterator]():void{
          }
        }
        let arr = [1,2,3]
        arr[Symbol.iterator]()
        for(let a of arr){}
      2.触发迭代器执行的场景
        yield *
        for..of
        new Set()/new map()
        Array.from()
        Promise.all()
        Promise.race()
    17)generator函数
      1.迭代器函数本质上就是generator函数
      手动实现一个类数组构造函数，并为该构造函数实现了Symbol.iterator接口(day04-10)
        function* foo(){
          yield "terry";
          yield "larry";
          yield "tom";
        }
      2.next参数
        next参数可以作为上一个yield表达式的返回值
    18)异步函数
      async function foo(){
        await axios.get(url1)
        await axios.get(url2)
      }
      foo()

      迭代器是通过generator函数实现的，generator还可以实现异步函数同步化，但是他的执行比较麻烦，通常借助co模块。async函数与generator函数非常相似，内置了执行器
    19)面向对象
      是ES5语法糖
      1.构造函数
        function Animal(name,age){
          this.name = name;
          this.age = age;
        }
        let animal = new Animal("大白",5);
        ------------------------------------
        class Animal{
          constructor(name,age){
            this.name = name;
            this.age = age;
          }
        }

        class 声明类的关键字
        Animal 类名，首字母大写，采用驼峰式命名
        {}  类体，其中可以包含：构造函数、成员属性、成员方法、静态方法、静态属性、静态代码块(node16才安装)
      2.实例对象
        通过构造函数创建的对象
        plain object 纯对象 ({},new Object())
        纯对象  直接指向object的对象
      3.原型方法 - 成员方法/实例方法
        class Animal{
          name;     //等价于animal.name,成员自身中存放
          sayName(){   //原型

          }
          sayAge(){

          }
        }
      4.构造函数方法 - 静态方法
        class Animal{
          sayAge(){

          }
          static foo(){   //类，通过类名直接调用

          }
        }
      5.继承
        原型链继承：子构造函数的原型指向父构造函数的实例
        借用构造函数：
          function Dog(name,age,gender){
            Animal.call(this,name,age);
            this.gender = gender;
          }
          Dog.prototype = new Animal();
          Dog.prototype.constructor = Dog;
          Dog.prototype.xxx    实例方法
          Dog.xxx    静态方法
          -----------------------------------
          class Dog extends Animal{
            constructor(name,age,gender){
              super(name,age)
              this.gender = gender;
            }
            sayGender(){

            }
          }
    20)模块化
      任何一个JS文件或者目录，都可以认为是一个模块。如果目录作为一个模块，那么，目录中应该出现package.json
      $ npm init -y
      $ npm install qs --save    //连接互联网，获取qs库？中央仓库，下载到当前目录的node_modules
      --save  本地安装并且将其安装信息写入到package.json中dependencies

      CommonJS 模块化
        社区提供的
        1.模块的暴露
          模块内部的变量，其他模块是无法访问的。如果想要其他模块进行访问，需要进行接口的暴露
          module.exports

          module是一个对象，每个模块都拥有这个对象;exports属性是用于将当前目录信息传递到其他模块，其默认值是一个空对象。
          module.exports.xxx
          或者
          module.exports = {}
        2.模块引用
          require(模块)
          1.当模块为自定义模块的时候，参数为文件路径
            require("./a.js")
          2.当模块为内置模块时，参数为模块名称
            require("http")
          3.当模块为第三方模块时，参数为模块名称，但是需要先进行模块的安装
            $ cnpm install 模块名 -save
            require("模块名")

            模块引入本质就是获取被引入模块的module.exports属性

      ES6 模块化
        官方提供的
        如果想要让nodejs支持es6模块化：node版本v14以上，package.json添加一个配置项 type:"module"

        模块暴露：
          export 声明
            多次使用，使用哪个变量暴露，就要使用哪个变量获取
          export default 
            仅可使用一次，可以使用任意变量名来获取
            一个模块中可以既使用export也使用export default

        模块导入
          import aa from "./a.js"

      CommonJS模块与ES6模块区别
         CommonJS 模块输出的是⼀个值的拷⻉，ES6 模块输出的是值的引⽤。 
         CommonJS 模块是运⾏时加载，ES6 模块是编译时输出接⼝。 
         CommonJS 模块的 require() 是同步加载模块，ES6 模块的 import 命令是异步加载， 有⼀个独⽴的模块依赖的解析阶段。
    21) Typescript  
      1.为什么要学
        面试需要，vue2(ES6) + vue3(Typescript)
        webpack - es6/ts -> Javascript
        React(ts)
      2.Javascript超集
        es5【标准】 < es6【标准】 < ts【社区】
        ts = es6 + 强类型
      3.数据类型
        es6是弱类型语言(一个变量的数据类型取决于值，数据类型可以改变)
          let a = undefined;
          a = 2;
          a = false;
        ts添加了强类型特性(一个变量的数据类型在声明的时候确定，变量的数据类型一旦确定无法修改)
          int a;
          a = 2;
          a = false;   //失败

        基本数据类型
          Number
          String
          boolean
          Symbol
          null
          undefined
        引用数据类型
          enum 枚举类型
          Object
            通过自定义类来校验对象
            let goods:Goods;
            goods = {}
          Array
            let arr: Array<string>;
            let set: Set<number>;
            let map: Map<string,number>;  //<键,值>
          Function
            函数通常存放在对象中，所以我们通过接口约束对象，进而约束函数
            interface Foo{
              login(username:string,password:string):User;
            }
      4.hello world
        ts --Typescript c--->js
        ts可以设置其编译级别(es5/es6)
        (1)tsc hello.ts --target es6
        (2)tsconfig.json

        1)安装typescript
          $cnpm install -g typescript
        2)编写ts代码
          文件名后缀 ts
          hello.ts
            let a:number;
            a = 1;
            console.log(a);
        3)编译
          $ tsc hello.ts
        4)执行
          $ node hello.js
      5.面向对象
        1)类
          class Animal{
            name:string;
            age:number;
          }
          class Dog extends Animal{
            color:string;
          }
          let dog:Dog;
          dog = new Dog();

          let animal:Animal;
          animal = new Dog();
          animal.color;  //无法访问
        2)泛型
          父类类型的引用指向子类对象,通过父类类型的引用只能访问父类中声明的特性。如果子类重写了父类的方法，通过父类类型引用来调用该方法，体现子类特性(本质还是调用子类中的方法【原型链中的就近原则】)
        3)重写
          子类中提供了一个与父类签名(函数名、参数类型、返回值)完全一致的方法
        4)抽象类
          如果一个类中出现了抽象方法，那么这个类一定是抽象类；抽象类无法实例化，存在的意义是为了让其他子类继承。
        5)接口
          抽象到极致的抽象类；接口中的方法全部为抽象方法，这些方法无需使用abstract来修饰

          接口是一种特殊的类，子类不再通过extends来继承，而是通过implement来继承
        6)访问修饰符
          private    只有在当前类中可以进行访问
          protect    在当前类、子类中可以访问
          public     所有地方都可以访问
      6.枚举  引用数据类型
        使⽤枚举我们可以定义⼀些带名字的常量。 使⽤枚举可以清晰地表达意图或创建⼀组有区别的⽤例。 TypeScript⽀持数字的和基于字符串的枚举
        6.1. 数字枚举
          如下，我们定义了⼀个数字枚举， Up 使⽤初始化为 1 。其余的成员会从1开始⾃动增⻓。
          enum Direction { 
            Up = 1,
            Down,
            Left, 
            Right 
          }
        6.2. 字符串枚举
          enum Direction { 
            Up = "UP", 
            Down = "DOWN", 
            Left = "LEFT", 
            Right = "RIGHT", 
          }


    





      
    数据类型的拓展  Symbol
    流程语句拓展 for-of
    Object拓展  Object.assign()、、、
    Array拓展
    函数拓展
    (面向对象) 类 继承 实例 静态
    单线程(事件循环)
      同步（1-2-3）
      异步（123 - ...）
    
    异步操作
      封装异步操作
        Promise 
        let queryStudent = axios.get(url);  
      异步操作同步化
        generator yield 函数
        async await 异步函数

      setTimeout
      setInterval
      ajax
        交互 http 请求（产生报文：请求行/请求头/请求体）
        let xhr = new XMLHttpRequest()
        xhr.open(url,method)    // GET /user/findAll http1.1 
        xhr.setRequestHeader()  // Content-Type 
        xhr.send()              // 请求体 
        xhr.onreadystatechange = function(){
          if(this.readyState === 4){
            if(this.tataus == 200){
              this.response
              console.log('finish');
            }
          }
        }
        console.log('end...');
      jquery
        $.get(url,function(resp){
          
        })

        异步操作的整体性
          let queryStudent = $.get(url,function(resp){
            resp
          })
  vue
  项目

6. 拓展阶段
  Typescript  (JS 强类型)
  nodejs / express /egg
  React




  组件库（基于html/css/jquery）
    bootstrap3  12栅格系统【 float 】
    bootstrap4  12栅格系统【 flex 】
    基础样式
    组件：表单 表格  模态框
  组件库（基于vue）
    elementui
    基础样式
    组件：表单 表格 模态框
  组件库（基于react）
    antdesign




------------细节点------------
1.JSON.parse()   凡是带parse的都是对字符串进行解析
  