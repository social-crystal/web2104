"use strict";
exports.__esModule = true;
/*
 * @Description:
 * @Author: zzj
 * @Date: 2021-09-24 11:50:41
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 17:34:41
 */
var Animal = /** @class */ (function () {
    function Animal() {
        this.id = 0;
        this.name = "";
    }
    return Animal;
}());
var a = {
    id: 1,
    name: "大白"
};
var b = {
    id: 2,
    name: "小黑",
    gender: "male"
};
console.log(a, b);
exports["default"] = {};
