/*
 * @Description: post方式
 * @Author: zzj
 * @Date: 2021-09-17 16:53:33
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-20 16:08:02
 */

//请求接口中的所有轮播图数据
//http://121.199.29.84:8001/swagger-ui.html#/
//1.初始化xhr
let xhr = new XMLHttpRequest();
//2.设置请求行
let url = "http://121.199.29.84:8001/carousel/saveOrUpdate";
let method = "POST";
xhr.open(method,url);
//3.设置请求头
let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJNalk9Iiwic3ViIjoiYWRtaW4yIiwiaXNzIjoiMDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjYiLCJpYXQiOjE2MzE4NjgxOTUsImF1ZCI6InJlc3RhcGl1c2VyIiwiZXhwIjoxNjMyMDQwOTk1LCJuYmYiOjE2MzE4NjgxOTV9.oHeGNVbT1E8lYhNkeKYlmfoxexYpyMSex8zOtyEUjH8";
xhr.setRequestHeader("Authorization",token);
xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
//4.设置请求体，并发送请求
let data = {
    name:"zzj测试",
    introduce:"---"
}
//name = 测试&introduce=----
xhr.send(Qs.stringify(data));
//5.监听响应
xhr.onreadystatechange = function(){
    if(this.readyState == 4){
        if(this.status ==200){
            let response = JSON.parse(this.response);
            console.log("success:",response);
        }else{
            console.log("error:",response);
        }
    }
}