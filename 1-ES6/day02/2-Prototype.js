/*
 * @Description: 原型
 * @Author: zzj
 * @Date: 2021-09-17 09:39:07
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-17 10:17:40
 */

function Animal(name) {
    this.name = name;
    
}
Animal.prototype.sayName = function () {
    console.log("my name is",this.name);
}


function Dog(name,color,age) {
    this.name = name;
    this,color = color;
    this.age = age;
}
//原型链继承
Dog.prototype =new Animal();
Dog.prototype.constructor = Dog;

Dog.prototype.sayColor = function(){
    console.log("my color is",this.color);
}

let dog = new Dog("大白","yellow",12)
dog.sayName();
dog.sayColor();
