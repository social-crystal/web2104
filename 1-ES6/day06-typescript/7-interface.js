/*
 * @Description: 接口
 * @Author: zzj
 * @Date: 2021-09-24 11:39:05
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-25 17:15:13
 */
var User = /** @class */ (function () {
    function User(id, name, gender) {
        this.name = name;
        this.id = id;
        this.gender = gender;
    }
    return User;
}());
var UserServiceImpl = /** @class */ (function () {
    function UserServiceImpl() {
    }
    UserServiceImpl.prototype.foo = function () {
    };
    UserServiceImpl.prototype.login = function (username, password) {
        return new User(1, "zzj", "女");
    };
    UserServiceImpl.prototype.register = function (user) {
    };
    return UserServiceImpl;
}());
