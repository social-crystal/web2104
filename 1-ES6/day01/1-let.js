/*
 * @Description: let
 * @Author: zzj
 * @Date: 2021-09-16 12:03:43
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-17 14:34:09
 */

function foo(){
    //var a = 0;
    let a = 0;
    for(let i=0;i<10;i++){
        a +=i;
    }
    console.log("i:",i);
    return a;
}
console.log(foo());