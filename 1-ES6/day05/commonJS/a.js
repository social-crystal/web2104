/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-23 11:12:22
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-24 14:30:01
 */
console.log("----正在加载a模块-----")
let msg = "这是a模块";
let num = 1;
let obj ={
    num:1
}
function foo() {
    console.log("hello module a");
}
setTimeout(() => {
    //++num;
    ++obj.num;
    //console.log("a模块中", num)
    console.log("a模块中", obj.num)
},1000)

//{msg,foo}
module.exports.msg = msg;
module.exports.foo = foo;
module.exports.num = num;
module.exports.obj = obj;