/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-10-19 09:46:07
 * @LastEditors: zzj
 * @LastEditTime: 2021-10-21 10:58:01
 */
import qs from "qs"

let obj = {
	name:"terry",
	age:23,
	gender:"male",
	telephone:1881234578
}
export default {
	url:"/user/register",
	data:qs.stringify(obj)
}