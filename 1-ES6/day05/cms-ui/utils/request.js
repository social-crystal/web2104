/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-24 15:11:09
 * @LastEditors: zzj
 * @LastEditTime: 2021-10-13 21:53:31
 */

import axios from "axios";
import qs from "qs";

let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJNalk9Iiwic3ViIjoiYWRtaW4yIiwiaXNzIjoiMDk4ZjZiY2Q0NjIxZDM3M2NhZGU0ZTgzMjYyN2I0ZjYiLCJpYXQiOjE2MzI0NjgwNTYsImF1ZCI6InJlc3RhcGl1c2VyIiwiZXhwIjoxNjMyNjQwODU2LCJuYmYiOjE2MzI0NjgwNTZ9.2fnhKIcE3bsmbDL9-Qxirl6NeUtwLuzS_e6cy1nQUno";

const instance = axios.create({
    baseURL: 'http://121.199.29.84:8001',
    timeout: 10000,
});
instance.interceptors.request.use((config)=>{
    config.headers.Authorization = token;
    return config;
})
instance.interceptors.response.use(({data:resp})=>{
    if(resp.status === 200){
        return resp;
    }
    //alert(resp.message);
    console.log("=====================",resp.message)
    return Promise.reject(resp);
},(error)=>{
    return Promise.reject(error)
})

export function get(url,params) {
    return instance.get(url,{ params });
}
export function post(url,data) {
    data = qs.stringify(data)
    return instance.post(url,data,{
        headers:{
            "Content-Type":"application/x-www-form-urlencoded"
        }
    })
}
export function postJSON(url,data) {
    return instance.post(url,data)
}
export function del(url,params) {
    return instance.delete(url,{params})
}
export default instance;