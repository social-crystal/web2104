/*
 * @Description: next参数
 * @Author: zzj
 * @Date: 2021-09-22 14:58:46
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-22 21:57:14
 */
function* foo() {
    let resp = yield "hello";
    console.log("foo-",resp);
    yield "world";
}
let iterator = foo();  //generator函数返回值是迭代器对象
iterator.next();  // {value:"hello",done:false}
//iterator.next();
iterator.next(888);