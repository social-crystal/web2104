/*
 * @Description: 
 * @Author: zzj
 * @Date: 2021-09-23 11:12:12
 * @LastEditors: zzj
 * @LastEditTime: 2021-09-24 14:31:24
 */
let a = require("./a");  //单例模型   (装饰器模式)
 a = require("./a");
 a = require("./a");

let b = require("./b");
let querystring = require("querystring"); //内置库
let qs = require("qs");  //第三方库

// console.log("index模块中：", a.num);
console.log("index模块中：", a.obj.num);
setTimeout(()=>{
    // console.log("index模块中：", a.num)
    console.log("index模块中：", a.obj.num)
},1500)
console.log(b);
let data ={
    id:1,
    name:"terry",
    age:12
}
console.log(querystring.stringify(data));
console.log(qs.stringify(data));
